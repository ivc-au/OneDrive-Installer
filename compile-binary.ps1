﻿#Requires -Version 2

# Source Required Libraries
Get-ChildItem -Path G:\OneDrive-Installer\ext\PS2EXE-GUI | Where-Object {$_.Name -like "ps2exe.ps1"} | ForEach-Object { . $_.FullName }

# Ensure binary path exist
If(-not(Test-Path G:\OneDrive-Installer\bin))
{
    New-Item -Path G:\OneDrive-Installer -Name bin -ItemType Directory -Force -Confirm:$false
}

# Remove old binary if exist
If(Test-Path G:\OneDrive-Installer\bin\IVC-OneDriveInstaller.exe)
{
    Remove-Item -Path G:\OneDrive-Installer\bin\IVC-OneDriveInstaller.exe -Confirm:$false -Force
}

# Compile to published binary
PS2EXE-GUI -inputFile 'G:\OneDrive-Installer\src\run-as-user.ps1' -outputFile 'G:\OneDrive-Installer\bin\IVC-OneDriveSetup.exe' -iconFile 'G:\OneDrive-Installer\lib\OneDriveSetup_ICON.ico' -noConfigfile $true -requireAdmin $true -copyright 'InvoCare Pty Limited' -title 'InvoCare OneDrive Installer' -x64 $true -version '1.0.0.3'
