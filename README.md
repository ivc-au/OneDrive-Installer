# OneDrive-Installer
 Installs and updates OneDrive version via SCCM.
 
 This is not intended for GUI use, as it returns a code to SCCM to validate the installation or failed the installation only.
 
 Install using:
 ```powershell
 Start-Process -FilePath C:\foo\IVC-SetupOneDrive.exe -Wait
 ```
