﻿#Requires -Version 2

# Source Required Libraries
#Get-ChildItem -Path G:\OneDrive-Installer\lib | Where-Object {$_.Name -like "*.ps1"} | ForEach-Object { . $_.FullName }

# Hide console action
###############################################################
Add-Type -Name WinAPI -Namespace Native -MemberDefinition '
[DllImport("Kernel32.dll")] 
public static extern IntPtr GetConsoleWindow();

[DllImport("user32.dll")]
public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);
'

function Show-Console
{
	$ConsoleHandle = [Native.WinAPI]::GetConsoleWindow()
	[Native.WinAPI]::ShowWindow($ConsoleHandle, 5) | Out-Null
}

function Hide-Console
{
	$ConsoleHandle = [Native.WinAPI]::GetConsoleWindow()
	[Native.WinAPI]::ShowWindow($ConsoleHandle, 0) | Out-Null
}
###############################################################

# Hide console output
Hide-Console

# Make sure download directory exists
If(-not(Test-Path "C:\IVCApps\APPS"))
{
    New-Item -Path "C:\IVCApps" -Name "APPS" -ItemType Directory -Force -Confirm:$false
}

# Download latest OneDrive installer
Switch(Test-Path "C:\IVCApps\APPS\OneDriveInstaller.exe")
{
    $false
    {
        Invoke-WebRequest -Uri https://go.microsoft.com/fwlink/?linkid=860984 -OutFile "C:\IVCApps\APPS\OneDriveInstaller.exe"
    }
    $true
    {
        If(-not([System.Diagnostics.FileVersionInfo]::GetVersionInfo("C:\IVCApps\APPS\OneDriveInstaller.exe").FileVersion -gt '19.123.0624.0005'))
        {
            Start-Job -Name WebReq -ScriptBlock {Invoke-WebRequest -Uri https://go.microsoft.com/fwlink/?linkid=860984 -OutFile "C:\IVCApps\APPS\OneDriveInstaller.exe"}
            Wait-Job -Name WebReq
            Start-Process -FilePath "C:\IVCApps\APPS\OneDriveInstaller.exe" -ArgumentList '/allusers /silent' -Wait 
        }
    }
}

# Check OneDrive installed for all users, else install globally
Switch(Test-Path HKLM:\SOFTWARE\WOW6432Node\Microsoft\OneDrive)
{
    $false
    {
        Start-Process -FilePath "C:\IVCApps\APPS\OneDriveInstaller.exe" -ArgumentList '/allusers /silent' -Wait
    }
    $true
    {
        [System.Version]$OneDriveVersion = (Get-ItemProperty HKLM:\SOFTWARE\WOW6432Node\Microsoft\OneDrive).Version
        If($OneDriveVersion -lt 19.123.0624.0005)
        {
            Start-Process -FilePath "C:\IVCApps\APPS\OneDriveInstaller.exe" -ArgumentList '/allusers /silent' -Wait
        }
    }
}

# Obtain OneDrive version number
[System.Version]$OneDriveVersion = (Get-ItemProperty HKLM:\SOFTWARE\WOW6432Node\Microsoft\OneDrive).Version

# Return true/false value if match for SCCM return value
If($OneDriveVersion -gt 19.123.0624.0005)
{
    # ERROR_SUCCESS
    return 0
}
Else
{
    # ERROR_INVALID_DATA
    return 13
}