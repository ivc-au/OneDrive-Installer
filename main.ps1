﻿#Requires -Version 2

# Source Required Libraries
Get-ChildItem -Path G:\OneDrive-Installer\lib | Where-Object {$_.Name -like "*.ps1"} | ForEach-Object { . $_.FullName }

# Download latest OneDrive installer
Switch(Test-Path "${RootPath}\exe\OneDriveInstaller.exe")
{
    $false
    {
        Invoke-WebRequest -Uri https://go.microsoft.com/fwlink/?linkid=860984 -OutFile "${RootPath}\exe\OneDriveInstaller.exe"
    }
    $true
    {
        If(-not([System.Diagnostics.FileVersionInfo]::GetVersionInfo("${RootPath}\exe\OneDriveInstaller.exe").FileVersion -gt '19.123.0624.0005'))
        {
            Start-Job -Name WebReq -ScriptBlock {Invoke-WebRequest -Uri https://go.microsoft.com/fwlink/?linkid=860984 -OutFile "${RootPath}\exe\OneDriveInstaller.exe"}
            Wait-Job -Name WebReq
            Start-Process -FilePath "${RootPath}\exe\OneDriveInstaller.exe" -ArgumentList '/allusers /silent' -Wait
        }
    }
}

# Check OneDrive installed for all users, else install globally
Switch(Test-Path HKLM:\SOFTWARE\WOW6432Node\Microsoft\OneDrive)
{
    $false
    {
        Start-Process -FilePath "${RootPath}\exe\OneDriveInstaller.exe" -ArgumentList '/allusers /silent' -Wait
    }
    $true
    {
        [System.Version]$OneDriveVersion = (Get-ItemProperty HKLM:\SOFTWARE\WOW6432Node\Microsoft\OneDrive).Version
        If($OneDriveVersion -lt 19.123.0624.0005)
        {
            Start-Process -FilePath "${RootPath}\exe\OneDriveInstaller.exe" -ArgumentList '/allusers /silent' -Wait
        }
    }
}

# Obtain OneDrive version number
[System.Version]$OneDriveVersion = (Get-ItemProperty HKLM:\SOFTWARE\WOW6432Node\Microsoft\OneDrive).Version

# Return true/false value if match for SCCM return value
If($OneDriveVersion -gt 19.123.0624.0005)
{
    $true
}
Else
{
    $false
}